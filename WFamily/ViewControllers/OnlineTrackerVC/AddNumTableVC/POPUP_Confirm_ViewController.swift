//
//  POPUP_Confirm_ViewController.swift
//  WFamily
//
//  Created by appentus technologies pvt. ltd. on 9/24/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit

protocol POPUP_Delegate {
    func func_EditNumber()
    func func_Confirm()
}

class POPUP_Confirm_ViewController: UIViewController {

    @IBOutlet weak var lbl_Mobile_Number:UILabel!
    
    @IBOutlet weak var view_PP_Confirm:UIView!
    @IBOutlet weak var btn_EditNumber:UIButton!
    @IBOutlet weak var btn_Confirm:UIButton!
    
    var str_Mobile_Number = ""
    
    var delegate:POPUP_Delegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_Mobile_Number.text = str_Mobile_Number
        
        func_RoundCorner(view_PP_Confirm, cornerRadius: 10)
        func_RoundCorner(btn_EditNumber, cornerRadius: btn_EditNumber.bounds.height/2)
        func_RoundCorner(btn_Confirm, cornerRadius: btn_Confirm.bounds.height/2)
    }
    
    @IBAction func btn_EditNumber(_ sender:UIButton) {
        self.view.removeFromSuperview()
        delegate?.func_EditNumber()
    }
    
    @IBAction func btn_Confirm(_ sender:UIButton) {
        self.view.removeFromSuperview()
        delegate?.func_Confirm()
    }
    
    @IBAction func btn_hide_view(_ sender:UIButton) {
        self.view.removeFromSuperview()
    }
    
    func func_RoundCorner(_ view:UIView,cornerRadius:CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
    }
    
}
