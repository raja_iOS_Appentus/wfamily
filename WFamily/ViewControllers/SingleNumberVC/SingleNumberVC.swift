//  SingleNumberVC.swift
//  WFamily
//  Created by Bullet Apps on 19/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.



import UIKit
import StoreKit



class SingleNumberVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView?
    
    var planDuration : String?
    var planDetail : String?
    var planPurchase : Bool? = false
    
    let arrTitle = ["WEEKLY PREMIUM","MONTHLY PREMIUM","3 MONTH PREMIUM"]
    let arrPrice = ["6.99","12.99","29.99"]
    let arrPlan = ["per week","per month","for three month"]
    let arrPlanDetail = ["Track Choosen Number For A Week","Track Choosen Number For Full Month","Track Choosen Number For Three Months"]
    
    let arrTopColor = [#colorLiteral(red: 0.6383562684, green: 0.385876596, blue: 0.9951258302, alpha: 1),#colorLiteral(red: 0.6263700128, green: 0.8068521619, blue: 0.3991550803, alpha: 1),#colorLiteral(red: 0.8998566866, green: 0.6605263948, blue: 0.1089573577, alpha: 1)]
    let arrBottomColor = [#colorLiteral(red: 0.2947509289, green: 0.3940112591, blue: 0.9951260686, alpha: 1),#colorLiteral(red: 0.2355087698, green: 0.7400442958, blue: 0.5624403954, alpha: 1),#colorLiteral(red: 0.9288108945, green: 0.5563133955, blue: 0.0527921319, alpha: 1)]
    
    var str_type_SUBSCRIPTIONS = ""
    var dict_ALL_SUBSCRIPTIONS = [String:Any]()
    var arr_ALL_SUBSCRIPTIONS = [[String:Any]]()
    var product_selected = SKProduct()
    var product_id = ""
    var is_PRODUCT_SELECT = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        func_Check_IAP()
    }
    
    func func_Check_IAP() {
        if (SKPaymentQueue.canMakePayments()) {
            func_HideHud_SV()
            func_ShowHud_SV()
            
            let productID:NSSet = NSSet(object:product_id_Weekly)
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self
            productsRequest.start()
            
            str_type_SUBSCRIPTIONS = "Weekly"
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0) {
            if (SKPaymentQueue.canMakePayments()) {
                self.func_HideHud_SV()
                self.func_ShowHud_SV()
                
                let productID:NSSet = NSSet(object: product_id_Monthly)
                let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
                productsRequest.delegate = self
                productsRequest.start()
                
                self.str_type_SUBSCRIPTIONS = "Monthly"
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+2.0) {
            if (SKPaymentQueue.canMakePayments()) {
                self.func_HideHud_SV()
                self.func_ShowHud_SV()
                
                let productID:NSSet = NSSet(object: product_id_3_Monthly)
                let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
                productsRequest.delegate = self
                productsRequest.start()
                self.str_type_SUBSCRIPTIONS = "Three Months"
            }
        }

    }
    
    func func_SuccessPayment() {
        func_ShowHud_SV()
        Model_SingleNumber.shared.func_SuccessPayment { (status, message) in
            DispatchQueue.main.async {
                self.func_HideHud_SV()
                
                UserDefaults.standard.set(true, forKey: kPremiumTrack)
                self.planPurchase = true
                self.tableView?.reloadData()
                
                self.tableView?.beginUpdates()
                self.tableView?.endUpdates()
            }
        }
    }
    
    
    
    //MARK: Custom Methods
    func setGradientBackground(topColor:UIColor,bottomColor:UIColor,vw:UIView) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = vw.bounds
        
        vw.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @objc func getPlanButtonClicked(_ sender:UIButton) {
        let dict_ALL_SUBSCRIPTIONS = arr_ALL_SUBSCRIPTIONS[sender.tag]
        let product_ID = dict_ALL_SUBSCRIPTIONS["SKProduct"] as! SKProduct
        buyProduct(product: product_ID)
        
        Model_SingleNumber.shared.plan_id = "\(dict_ALL_SUBSCRIPTIONS["productIdentifier"]!)".components(separatedBy: "_")[1]
        
//        UserDefaults.standard.set(true, forKey: kPremiumTrack)
//        self.tableView?.beginUpdates()
//        planPurchase = true
        
        
        switch sender.tag {
        case 0:
            planDuration = "FOR 1 WEEK"
            planDetail = "Track Single Number For Full Week"
        case 1:
            planDuration = "FOR 1 MONTH"
            planDetail = "Track Single Number For Full Month"
        default:
            planDuration = "FOR 3 MONTHS"
            planDetail = "Track Single Number For Three Months"
        }

//        self.tableView?.endUpdates()
    }
    
    //MARK: IBActions
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UITableView DataSource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if planPurchase! {
            return 1
        } else {
            return arr_ALL_SUBSCRIPTIONS.count
        }
//        return arrTitle.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if arr_ALL_SUBSCRIPTIONS.count > indexPath.row {
        if !planPurchase! {
            let planCell = tableView.dequeueReusableCell(withIdentifier: "planCell", for: indexPath)
            
            let baseView = planCell.viewWithTag(10) ?? UIView()
            let titleLbl = planCell.viewWithTag(20) as! UILabel
            let planLbl = planCell.viewWithTag(30) as! UILabel
            let priceLbl = planCell.viewWithTag(40) as! UILabel
            let planDetailLbl = planCell.viewWithTag(50) as! UILabel
            let getPlanBtn = planCell.viewWithTag(60) as? UIButton
            let sideLbl = planCell.viewWithTag(90) as! UILabel
            let currency_symbol = planCell.viewWithTag(121) as! UILabel
            
            sideLbl.roundCorners(corners: [.bottomRight], radius: 15.0)
            
            if indexPath.row == 2 {
                sideLbl.isHidden = false
            }else {
                sideLbl.isHidden = true
            }
            
            getPlanBtn?.layer.cornerRadius = (getPlanBtn?.frame.size.height)!/2
            getPlanBtn?.tag = indexPath.row
            getPlanBtn?.addTarget(self, action: #selector(getPlanButtonClicked(_:)), for: .touchUpInside)
            
            let size = Utility.getIphoneDevice(vc: self)
            if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
                getPlanBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 17.0)
            }
            
            // Shadow and Radius for Button
            getPlanBtn?.layer.shadowColor = UIColor.black.cgColor
            getPlanBtn?.layer.shadowOpacity = shadowOpacity
            getPlanBtn?.layer.shadowRadius = shadowRadius
            getPlanBtn?.layer.shadowOffset = CGSize.zero
            getPlanBtn?.layer.masksToBounds = false
            
            let dict_ALL_SUBSCRIPTIONS = arr_ALL_SUBSCRIPTIONS[indexPath.row]
            
            let currencySymbol = "\(dict_ALL_SUBSCRIPTIONS["currencySymbol"] ?? "")"
            titleLbl.text = "\(dict_ALL_SUBSCRIPTIONS["localizedTitle"] ?? "")"
            priceLbl.text = "\(dict_ALL_SUBSCRIPTIONS["localizedDescription"] ?? "")"
            planLbl.text = "\(currencySymbol) \(dict_ALL_SUBSCRIPTIONS["price"] ?? "")"
            planDetailLbl.text = "\(dict_ALL_SUBSCRIPTIONS["plan"] ?? "")"
            
            
            baseView.layer.cornerRadius = cornerRadius
            self.setGradientBackground(topColor: arrTopColor[indexPath.row], bottomColor: arrBottomColor[indexPath.row], vw: baseView)
            
            return planCell
        } else {
            let planCell = tableView.dequeueReusableCell(withIdentifier: "planActivationCell", for: indexPath)
            
            let baseView = planCell.viewWithTag(10) ?? UIView()
            let sideLbl = planCell.viewWithTag(20) as! UILabel
            let planDurationLbl = planCell.viewWithTag(30) as! UILabel
            let planDetailLbl = planCell.viewWithTag(40) as! UILabel
            
            baseView.layer.cornerRadius = cornerRadius
            sideLbl.roundCorners(corners: [.bottomRight], radius: 15.0)
            planDurationLbl.text = planDuration ?? ""
            planDetailLbl.text = planDetail ?? ""
            
            return planCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if planPurchase! {
            return 280
//        } else {
//            return 260
//        }
        
//        switch indexPath.row {
//        case 0,1,2:
//            return planPurchase == true ? 0 : UITableView.automaticDimension
//        default:
//            return planPurchase == true ? UITableView.automaticDimension : 0
//        }
    }

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}



// InAppPurchase methods
extension SingleNumberVC : SKProductsRequestDelegate,SKPaymentTransactionObserver{
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let count : Int = response.products.count
        
        func_HideHud_SV()
        func_ShowHud_SV()
        
        if (count>0) {
            let validProduct : SKProduct = response.products[0] as SKProduct
            
            var dict_SUBSCRIPTIONS = [
                "SKProduct":validProduct,
                "localizedTitle":"\(validProduct.localizedTitle)",
                "localizedDescription":"\(validProduct.localizedDescription)",
                "price":"\(validProduct.price)",
                "currencyCode":"\(validProduct.priceLocale.currencyCode ?? "")",
                "productIdentifier":"\(validProduct.productIdentifier)",
                "currencySymbol":"\(validProduct.priceLocale.currencySymbol ?? "")"] as [String : Any]
            print(dict_SUBSCRIPTIONS)
            
            if validProduct.localizedTitle.contains("Weekly Plan") {
                let currencySymbol = validProduct.priceLocale.currencySymbol!
                let price_per_day = String(format: "%.2f", Float64(validProduct.price)/7)
                dict_SUBSCRIPTIONS["plan"] = "\(currencySymbol) \(price_per_day) /day (7 days)"
                dict_ALL_SUBSCRIPTIONS["Weekly"] = dict_SUBSCRIPTIONS
            } else if validProduct.localizedTitle.contains("Monthly Plan") {
                let currencySymbol = validProduct.priceLocale.currencySymbol!
                let price_per_day = String(format: "%.2f", Float64(validProduct.price)/30)
                dict_SUBSCRIPTIONS["plan"] = "\(currencySymbol) \(price_per_day) /day (30 days)"
                dict_ALL_SUBSCRIPTIONS["Monthly"] = dict_SUBSCRIPTIONS
            } else if validProduct.localizedTitle.contains("Three Months") {
                let currencySymbol = validProduct.priceLocale.currencySymbol!
                let price_per_day = String(format: "%.2f", Float64(validProduct.price)/90)
                dict_SUBSCRIPTIONS["plan"] = "\(currencySymbol) \(price_per_day) /day (90 days)"
                dict_ALL_SUBSCRIPTIONS["ThreeMonthly"] = dict_SUBSCRIPTIONS
            }
            
            func_set_ALL_SUBSCRIPTIONS()
            func_HideHud_SV()
        } else {
            func_HideHud_SV()
        }
    }
    
    
    
    func func_set_ALL_SUBSCRIPTIONS() {
        if let dict_SUBSCRIPTIONS = dict_ALL_SUBSCRIPTIONS["Weekly"] as? [String:Any] {
            if arr_ALL_SUBSCRIPTIONS.count > 0 {
                if arr_ALL_SUBSCRIPTIONS.contains(where:{ $0["productIdentifier"] as! String == product_id_Weekly}) {
                    print("exist weekly")
                } else {
                    arr_ALL_SUBSCRIPTIONS.append(dict_SUBSCRIPTIONS)
                }
            } else {
                arr_ALL_SUBSCRIPTIONS.append(dict_SUBSCRIPTIONS)
            }
        }
        
        if let dict_SUBSCRIPTIONS = dict_ALL_SUBSCRIPTIONS["Monthly"] as? [String:Any] {
            if arr_ALL_SUBSCRIPTIONS.count > 0 {
                if arr_ALL_SUBSCRIPTIONS.contains(where:{ $0["productIdentifier"] as! String == product_id_Monthly}) {
                    print("exist Monthly")
                } else {
                    arr_ALL_SUBSCRIPTIONS.append(dict_SUBSCRIPTIONS)
                }
            } else {
                arr_ALL_SUBSCRIPTIONS.append(dict_SUBSCRIPTIONS)
            }
        }
        
        if let dict_SUBSCRIPTIONS = dict_ALL_SUBSCRIPTIONS["ThreeMonthly"] as? [String:Any] {
            if arr_ALL_SUBSCRIPTIONS.count > 0 {
                if arr_ALL_SUBSCRIPTIONS.contains(where:{ $0["productIdentifier"] as! String == product_id_3_Monthly}) {
                    print("exist ThreeMonthly")
                } else {
                    arr_ALL_SUBSCRIPTIONS.append(dict_SUBSCRIPTIONS)
                }
            } else {
                arr_ALL_SUBSCRIPTIONS.append(dict_SUBSCRIPTIONS)
            }
        }
        tableView?.reloadData()
    }
    
    
    
    func buyProduct(product: SKProduct) {
        func_ShowHud_SV()
        
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().add(self)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]){
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction {
                print(trans.transactionState)
                
                switch trans.transactionState {
                case .deferred:
                    self.func_HideHud()
//                    func_show_error("InAppPurchase Deffered")
                    
                case .purchased:
//                    UserDefaults.standard.set(true, forKey: "is_InAppPurchased")
                    
                    product_id_purchased = product_id
                    
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
//                    func_show_success("In App Payment Success")
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                        self.func_HideHud()
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                    self.func_SuccessPayment()
                    
                    break
                case .failed:
                    self.func_HideHud()
//                    func_show_error("InAppPurchase Failed")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    break
                case .restored :
                    product_id_purchased = product_id
                    
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
//                    func_show_success("In App Payment Success")
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                        self.func_HideHud()
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                    self.func_SuccessPayment()
                    
                    break
                default:
                    break
                }
            }
        }
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "product_id_purchased"), object: nil)
        
        func_HideHud_SV()
    }
    
    func restore() {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
        //        func_ShowHud()
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        for transact:SKPaymentTransaction in queue.transactions {
            print(transact.transactionState)
            
            if transact.transactionState == .restored {
                let t: SKPaymentTransaction = transact as SKPaymentTransaction
                _ = t.payment.productIdentifier as String
                SKPaymentQueue .default().finishTransaction(transact)
            } else {
                
            }
            
        }
    }
    
    private func paymentQueue(queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: NSError){
        for transaction:SKPaymentTransaction  in queue.transactions {
            if transaction.transactionState == SKPaymentTransactionState.restored {
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            }
        }
    }
    
    private func request(request: SKRequest, didFailWithError error: NSError){
        print(error)
    }
    
}


