//  AppDelegate.swift
//  WFamily

//  Created by Bullet Apps on 17/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.


import UIKit
import UserNotifications
import Alamofire

import Firebase
import FirebaseMessaging

import Fabric
import Crashlytics

var center = UNUserNotificationCenter.current()
var app:UIApplication!
var expriryDate:Date!
var duration_time = ""


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?
    
    var timer: Timer?
    var sec = 0
    var totalSecondsTracking = String()
    var totalSeconds = 0
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Fabric.with([Crashlytics.self])
        logUser()
        
        app = application
        func_register()
        
        center.removeAllDeliveredNotifications()
        
//        askForNotificationPermission()
        
        return true
    }
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("user@fabric.io")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
        
//        Crashlytics.sharedInstance().crash()
    }
    
    
    
    func func_register() {
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            center.requestAuthorization(options: authOptions) { (granted, error) in
                
            }
            
            center.delegate = self
//            center.requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            app.registerUserNotificationSettings(settings)
        }
        
        app.registerForRemoteNotifications()
    }
    
    // MARK: Firebase messeging Delegates
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        fcmTokenStr = fcmToken
        
        self.createUserOnWhatsAppStatusServer()
        
        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name:Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
    }
    
    //MARK: User notification Delegates
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        center.removeAllDeliveredNotifications()
        
        if notification.request.content.body.contains("Of") {
            if let notification_offline = UserDefaults.standard.object(forKey:"notification_offline") as? Bool {
                if notification_offline {
                    completionHandler([.alert, .badge, .sound])
                } else {
                    completionHandler([])
                }
            } else {
                completionHandler([.alert, .badge, .sound])
            }

        } else {
            completionHandler([.alert, .badge, .sound])
        }
        
        print(notification.request.content.title)
        print(notification.request.content.body)
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "track_status"), object:notification.request.content.body)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.title)
        print(response.notification.request.content.body)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        if UserDefaults.standard.bool(forKey: kNumberTrack) {
            let currentTime = Utility.setCurrentTime_Date()
            UserDefaults.standard.set(currentTime, forKey: kCurrentTrackDateTime)
            
            UserDefaults.standard.set(totalSeconds, forKey: kTotalSeconds)
            
            self.totalSeconds = 0
            self.sec = 0
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        /*
        if UserDefaults.standard.bool(forKey: kNumberTrack) {
            
            //to get current time when timer pause
            totalSecondsTracking = UserDefaults.standard.string(forKey: kCurrentTrackDateTime) ?? ""
            print(totalSecondsTracking)
            
            //to get total minutes of tracking
            let lastTotalSecondsTracking = UserDefaults.standard.integer(forKey: kTotalSeconds)
            print(lastTotalSecondsTracking)
            
            //to get time difference between two dates
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss a"
            //dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.calendar = Calendar.current
            dateFormatter.timeZone = TimeZone.current
            
            let nowTime = dateFormatter.string(from: Date())
            let current = dateFormatter.date(from: nowTime)
            let lastTrackTime = dateFormatter.date(from: totalSecondsTracking) ?? Date()
            print(lastTrackTime)
            
            let timeInterval = current?.timeIntervalSince(lastTrackTime)
            print(timeInterval ?? 0)
            
            totalSeconds = Int(lastTotalSecondsTracking) + Int((timeInterval ?? 0))
            print(totalSeconds)
            
            let totalMinute = Int((totalSeconds) / 60) % 60
            print(totalMinute)
            
            if totalMinute >= 480 {
                //pause the timer and disable the track button
                UserDefaults.standard.set(true, forKey: kTrialExpired)
                
                if let disable = disableTimerWithTrackBtn {
                    disable(totalSeconds)
                }
            } else {
                //start timer with addition of last track-time
                if let resume = resumeTimerWithLastTrackTime {
                    resume(totalSeconds)
                }
            }
        }
        */
        
    }
    
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "didBecomeActive"), object: nil)
        
        
        
//        if UserDefaults.standard.bool(forKey: kNumberTrack) {
//            //to get current time when timer pause
//            totalSecondsTracking = UserDefaults.standard.string(forKey: kCurrentTrackDateTime) ?? ""
//            //print(totalSecondsTracking)
//
//            //to get total minutes of tracking
//            let lastTotalSecondsTracking = UserDefaults.standard.integer(forKey: kTotalSeconds)
//            //print(lastTotalSecondsTracking)
//
//            //to get time difference between two dates
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            dateFormatter.locale = Locale(identifier: "fr_FR")
//            //dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
//
//            let nowTime = dateFormatter.string(from: Date())
//            //print(nowTime)
//
//            let current = dateFormatter.date(from: nowTime)
//            //print(current ?? Date())
//
//            let lastTrackTime = dateFormatter.date(from: totalSecondsTracking) ?? Date()
//            //print(lastTrackTime)
//
//            let timeInterval = current?.timeIntervalSince(lastTrackTime)
//            //print(timeInterval ?? 0)
//
//            totalSeconds = Int(lastTotalSecondsTracking) + Int((timeInterval ?? 0))
//            //print(totalSeconds)
//
//            let totalMinute = Int((totalSeconds) / 60)
//            //print(totalMinute)
//
//            if totalMinute >= 480 {
//                //pause the timer and disable the track button
//                UserDefaults.standard.set(true, forKey: kTrialExpired)
//
//                if let disable = disableTimerWithTrackBtn {
//                    //disable(totalSeconds)
//                    disable()
//                }
//            } else {
//                if let resume = resumeTimerWithLastTrackTime {
//                    resume(totalSeconds)
//                }
//
//            }
//
//        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
       
    }

    func createUserOnWhatsAppStatusServer() {
        let parameter = ["device_id"   : UIDevice.current.identifierForVendor?.uuidString,
                          "token"       : fcmTokenStr ?? "",
                          "device_type" : "2"] as! [String:String]
        print(parameter)
        
        let headers = ["password" : "123456"]
        
        Alamofire.request(kBaseUrl + "createUser", method: .post, parameters: parameter,headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                let dict_response = response.result.value as! [String:Any]
                
                let arr_result = dict_response["result"] as! [[String:Any]]
                Model_Splash.shared.id = "\(arr_result[0]["id"] ?? "")"
                Model_Splash.shared.device_id = "\(arr_result[0]["device_id"] ?? "")"
                Model_Splash.shared.token = "\(arr_result[0]["token"] ?? "")"
                Model_Splash.shared.device_type = "\(arr_result[0]["device_type"] ?? "")"
                Model_Splash.shared.plan_id = "\(arr_result[0]["plan_id"] ?? "")"
                Model_Splash.shared.start = "\(arr_result[0]["start"] ?? "")"
                Model_Splash.shared.end = "\(arr_result[0]["end"] ?? "")"
                
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
            
        }
        
//        Alamofire.upload(multipartFormData: { (multiData) in
//
//            for (key,value) in parameter {
//                multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
//            }
//
//        }, to: kBaseUrl + "createUser", method: .post, headers:["password" : "123456"]) { (result) in
//
//            switch result {
//            case .success(let upload,_,_):
//
//                upload.responseJSON(completionHandler: { (response) in
//                    print(response.result.value ?? "no response")
//                    print(response.response?.statusCode ?? 00)
//
//                    let dict = response.result.value as? [String:Any]
//                    print(dict?["message"] ?? "message")
//                    print(dict?["status"] ?? "status")
//                    print(dict?["user_id"] ?? "user_id")
//
//                    self.subscribeUser(userID: "\(dict?["user_id"] ?? 0)")
//                })
//
//            case .failure(let encodingError):
//                print(encodingError)
//            }
//
//        }
        
    }
    
//    func subscribeUser(userID:String) {
//
////        user_id:1
////        mobile_number:917877838890
////        name:subham
////        code:91
//
//        let parameter = ["user_id"       : userID,
//                          "name"          : "Sameer",
//                          "mobile_number" : "919413039220"]
//        print(parameter)
//
//        let headers = ["password" : "123456"]
//
//        Alamofire.request(kBaseUrl + kSubscribeUser, method: .post, parameters: parameter,headers: headers).responseJSON { (response) in
//            switch response.result {
//            case .success:
//                let dict = response.result.value as? [String:Any]
//                print(dict)
//
//                print(dict?["message"] ?? "message")
//                print(dict?["status"] ?? "status")
//                print(dict?["user_id"] ?? "user_id")
//
//                self.subscribeUser(userID: "\(dict?["user_id"] ?? 0)")
//
//                break
//
//            case .failure(let error):
//                print(error.localizedDescription)
//                break
//            }
////        Alamofire.upload(multipartFormData: { (multiData) in
////
////            for (key,value) in parameters {
////                multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
////            }
////
////        }, to: kSubscribeUser, method: .post, headers: kHeader) { (result) in
////
////            switch result {
////            case .success(let upload,_,_):
////
////                upload.responseJSON(completionHandler: { (response) in
////                    print(response.result.value ?? "no response")
////                })
////
////            case .failure(let encodingError):
////                print(encodingError)
////            }
////
//        }
//
//    }
    
    
    
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        print(jsonResponse)
        
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
                return formatter.date(from: expiresDate)
            }
            return nil
        } else {
            return nil
        }
    }
    
}



extension Date {
    func offsetFrom(date : Date) -> String {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day,.month]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);
        
        return "\(difference.day ?? 0)"
        
        let seconds = "\(difference.second ?? 0)"
        let minutes = "\(difference.minute ?? 0)" + " " + seconds
        let hours = "\(difference.hour ?? 0)" + " " + minutes
        let days = "\(difference.day ?? 0)" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        
        return ""
    }
    
}


